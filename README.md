![N|Solid](logo_ioasys.png)

# Desafio - Projeto Empresas

### Como executar
Para executar o projeto, basta **Importar/Abrir** no Android Studio e clicar em **Run**. ;D

### Justificativas
- Usei os componentes do **JetPack** para fazer boa parte do aplicativo, pois sempre é recomendado usar 
dependências mantidas pela própria Google e o time do Android. Dentre elas, temos:
    - **Navigation** - Usei para fazer a navegação entre as 3 telas do aplicativo com uma única 
    Activity (arquitetura **Single Activity**). Isso facilita focar nos fluxos do aplicativo com 
    de gráficos de navegação, transições, passagem de dados, fácil deep link, etc.
    - **ViewModel + LiveData** - Usei para aplicar arquitetura **MVVM** ao projeto. O principal ponto do 
    ViewModel e LiveData é o fato de serem consciente de ciclo de vida (como a maioria dos 
    componentes do JetPack), diminuindo drasticamente os problemas de vazamento de memória, etc;
    - **Hilt** - Usei como uma POC para aplicar injeção de dependência, pois ficou estável recentemente 
    e eu só tinha utilizado **Dagger** e **Koin** nos projetos passados, e eu fiquei satisfeito com o que eu
    vi, realmente é muito menos boilerplate comparado com o Dagger puro.
- Migrei os arquivos de compilação de **Groovy** para **Gradle Kotlin DSL**, pois Kotlin é mais legível 
e tem autocomplete da IDE (amém), e também claro, irá substituir o Groovy no futuro, logo é interessante 
já ir se familiarizando;
- Usei o plugin do **Ktlint** no projeto para melhorar o lint do código e também 
para usufruir dos pre-commit hooks (me ajudam muito a seguir o Code Style nos meus commits);
- Usei **Kotlin Coroutines** para as operações assíncronas, pois é recurso prático, leve e simples 
que já vem com o Kotlin e tem total integração com os componentes do JetPack e outras dependências 
que eu utilizei;
- Usei **Retrofit** por ser uma biblioteca de client HTTP massivamente utilizada no Android (e também 
estava como uma dica do desafio), porém eu tenho interesse em testar o **Ktor**, pois acho 
interessante a ideia de tornar os códigos mais convencionais com Kotlin (com Kotlin DSL, Extensions, 
Higher-Order Functions, etc) ao invês de usar convenções Java no Kotlin (como o Retrofit acaba nos 
obrigando a fazer).
- Usei o template de Android do **Bitbucket Pipelines** como uma forma rápida do projeto ter uma 
Integração Contínua, e se sobrasse tempo, eu iria acrescentar deployment e reports de cobertura de 
testes;
- Usei o **Jira** para gerenciar as minhas tarefas durante o desenvolvimento 
(ta tudo linkadinho nos commits);
- Utilizei o **Glide** para download de imagens por eu sentir que ele é superior ao Picasso e também 
porque era uma dica do projeto;
- Usei **Espresso** nos testes de instrumentação mais por preferência pessoal e também porque já vem ao 
criar o projeto do zero pelo Android Studio;
- Usei **ConstraintLayout** nos layouts, pois é muito prático para posicionar as Views filhas na tela e 
também é uma recomendação da própria Google;
- Usei **Material Components** por fornecer os principais componentes da guideline de design do mais 
utilizada no Android; 

### O que faria se tivesse mais tempo...
- Iria melhorar a persistência e uso dos dados de autenticação;
- Iria melhorar a cobertura dos testes unitários existentes;
- Iria aplicar o **JUnit 5** nos testes;
- Iria integrar o **JaCoCo** para reportar no **Sonarcloud** via Bitbucket Pipelines;
- Iria adicionar mais testes de instrumentação e melhorar os existentes;
- Iria melhorar os temas, estilos e layouts do projeto (principalmente o tema noturno);
- Iria habilitar a minificação e ofuscação do artefato de release;
- Iria adicionar **Fastlane** para fazer deploys em alguma plataforma de distribuição (**Firebase App 
Distribution**, **AppCenter**, etc);

### Autor

- [Adriano Teles](https://github.com/adrianotelesc)
