package com.adrianotelesc.empresas

import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.adrianotelesc.empresas.signin.SignInFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class SignInFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

    @Before
    fun setUp() {
        launchFragmentInHiltContainer<SignInFragment>(themeResId = R.style.Theme_Empresas) {
            navController.setGraph(R.navigation.nav_graph)

            Navigation.setViewNavController(requireView(), navController)
        }
    }

    @Test
    fun testSignInSuccessful() {
        onView(withId(R.id.email_input)).perform(typeText("testeapple@ioasys.com.br"))
        onView(withId(R.id.password_input)).perform(typeText("12341234"))
        onView(withId(R.id.sign_in_button)).perform(click())
        Thread.sleep(2000) // Workaround
        assertThat(navController.currentDestination?.id, `is`(R.id.enterprisesFragment))
    }

    @Test
    fun testSignInFailure() {
        onView(withId(R.id.sign_in_button)).perform(click())
        Thread.sleep(2000) // Workaround
        onView(withId(R.id.error)).check(matches(isDisplayed()))
    }
}
