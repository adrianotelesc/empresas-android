package com.adrianotelesc.empresas.enterprises

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.adrianotelesc.empresas.data.FakeEnterpriseRepositoryImpl
import com.adrianotelesc.empresas.data.FakeUserRepositoryImpl
import com.adrianotelesc.empresas.data.Result
import com.adrianotelesc.empresas.util.MainCoroutineRule
import com.adrianotelesc.empresas.util.getOrAwaitValue
import com.adrianotelesc.empresas.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class EnterprisesViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val userRepo = FakeUserRepositoryImpl()

    private val enterpriseRepo = FakeEnterpriseRepositoryImpl()

    private lateinit var viewModel: EnterprisesViewModel

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = EnterprisesViewModel(userRepo, enterpriseRepo)
    }

    @Test
    fun search_setsSearchEvent() {
        viewModel.filterEvent.observeForTesting {
            viewModel.filter("empresa")

            assertThat(viewModel.filterEvent.getOrAwaitValue(), not(nullValue()))
        }
    }

    @Test
    fun search_loadingEmpty() {
        enterpriseRepo.testDispatcher.pauseDispatcher()

        viewModel.filterEvent.observeForTesting {
            viewModel.filter("empresa")

            assertThat(viewModel.enterprises.getOrAwaitValue(), `is`(Result.Loading))

            enterpriseRepo.testDispatcher.resumeDispatcher()

            assertThat(viewModel.enterprises.getOrAwaitValue(), `is`(Result.Success(emptyList())))
        }
    }

    @Test
    fun search_loadingNotEmpty() {
        enterpriseRepo.testDispatcher.pauseDispatcher()

        viewModel.filterEvent.observeForTesting {
            viewModel.filter("Fluoretiq")

            assertThat(viewModel.enterprises.getOrAwaitValue(), `is`(Result.Loading))

            enterpriseRepo.testDispatcher.resumeDispatcher()

            assertThat(viewModel.enterprises.getOrAwaitValue(), not(Result.Success(emptyList())))
        }
    }
}
