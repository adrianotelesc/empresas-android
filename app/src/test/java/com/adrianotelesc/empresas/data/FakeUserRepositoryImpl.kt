package com.adrianotelesc.empresas.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.adrianotelesc.empresas.util.isFilled
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher

@ExperimentalCoroutinesApi
class FakeUserRepositoryImpl : UserRepository {
    val testDispatcher = TestCoroutineDispatcher()
    override val auth: Map<String, String> get() = mapOf()

    override fun signIn(emailAddress: String?, password: String?): LiveData<Result<Unit>> =
        liveData(testDispatcher) {
            emit(if (emailAddress.isFilled() && password.isFilled()) Result.Success(Unit) else Result.Error(Exception()))
        }
}
