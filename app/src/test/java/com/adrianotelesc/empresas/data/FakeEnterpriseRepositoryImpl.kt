package com.adrianotelesc.empresas.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher

@ExperimentalCoroutinesApi
class FakeEnterpriseRepositoryImpl : EnterpriseRepository {
    val testDispatcher = TestCoroutineDispatcher()

    override fun filter(
        auth: Map<String, String>,
        query: String
    ): LiveData<Result<List<Enterprise>>> =
        liveData(testDispatcher) {
            emit(
                Result.Success(
                    enterprises.filter { it.name.contains(query) }
                )
            )
        }

    companion object {
        private val enterprises = listOf(
            Enterprise(
                0L,
                "Fluoretiq Limited",
                "FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.  ",
                "/uploads/enterprise/photo/1/240.jpeg",
                "UK",
                "Bristol",
                Enterprise.Type("Putaria")
            ),
            Enterprise(
                0L,
                "Little Bee Community",
                "Service and Product Design, Responsible Business",
                "/uploads/enterprise/photo/1/240.jpeg",
                "UK",
                "London",
                Enterprise.Type("Putaria")
            )
        )
    }
}
