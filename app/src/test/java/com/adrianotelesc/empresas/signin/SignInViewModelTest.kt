package com.adrianotelesc.empresas.signin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.adrianotelesc.empresas.data.FakeUserRepositoryImpl
import com.adrianotelesc.empresas.data.Result
import com.adrianotelesc.empresas.util.Event
import com.adrianotelesc.empresas.util.MainCoroutineRule
import com.adrianotelesc.empresas.util.getOrAwaitValue
import com.adrianotelesc.empresas.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SignInViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val userRepo = FakeUserRepositoryImpl()

    private lateinit var viewModel: SignInViewModel

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = SignInViewModel(userRepo)
    }

    @Test
    fun signIn_setsSignInEvent() {
        val observer = Observer<Event<Unit>> {}
        try {

            viewModel.signInEvent.observeForever(observer)

            viewModel.signIn()

            val value = viewModel.signInEvent.getOrAwaitValue()
            assertThat(value.getContentIfNotHandled(), not(nullValue()))
        } finally {
            viewModel.signInEvent.removeObserver(observer)
        }
    }

    @Test
    fun getSignInResult_loadingSuccess() {
        userRepo.testDispatcher.pauseDispatcher()

        viewModel.signInResult.observeForTesting {
            viewModel.emailAddress.value = "adriano.telesc@gmail.com"
            viewModel.password.value = "112233000"
            viewModel.signIn()

            assertThat(viewModel.signInResult.getOrAwaitValue(), `is`(Result.Loading))

            userRepo.testDispatcher.resumeDispatcher()

            assertThat(viewModel.signInResult.getOrAwaitValue(), `is`(Result.Success(Unit)))
        }
    }

    @Test
    fun getSignInResult_loadingError() {
        userRepo.testDispatcher.pauseDispatcher()

        viewModel.signInResult.observeForTesting {
            viewModel.emailAddress.value = ""
            viewModel.password.value = ""
            viewModel.signIn()

            assertThat(viewModel.signInResult.getOrAwaitValue(), `is`(Result.Loading))

            userRepo.testDispatcher.resumeDispatcher()

            assertThat(viewModel.signInResult.getOrAwaitValue(), not(Result.Success(Unit)))
        }
    }
}
