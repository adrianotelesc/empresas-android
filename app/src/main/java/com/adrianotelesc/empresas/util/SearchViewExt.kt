package com.adrianotelesc.empresas.util

import androidx.appcompat.widget.SearchView

inline fun SearchView.onQueryTextSubmit(crossinline action: (String) -> Boolean) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean = action(query ?: "")

        override fun onQueryTextChange(newText: String?): Boolean = false
    })
}
