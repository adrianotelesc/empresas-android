package com.adrianotelesc.empresas.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.progressindicator.LinearProgressIndicator

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("imageUrl", "error")
    fun loadImage(imageView: ImageView, url: String, error: Drawable) {
        Glide.with(imageView)
            .load(url)
            .error(error)
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("isLoading")
    fun progress(progress: LinearProgressIndicator, isLoading: Boolean) {
        if (isLoading) {
            progress.show()
        } else {
            progress.hide()
        }
    }
}
