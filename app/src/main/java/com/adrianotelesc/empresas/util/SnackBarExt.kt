package com.adrianotelesc.empresas.util

import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun View.longSnack(@StringRes resId: Int) {
    Snackbar.make(this, resId, Snackbar.LENGTH_LONG).show()
}

fun Fragment.longSnack(@StringRes resId: Int) {
    requireView().longSnack(resId)
}
