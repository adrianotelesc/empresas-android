package com.adrianotelesc.empresas.util

fun String?.isFilled() = !isNullOrEmpty() && !isNullOrBlank()
