package com.adrianotelesc.empresas.enterprisedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.adrianotelesc.empresas.databinding.FragmentEnterpriseDetailBinding

class EnterpriseDetailFragment : Fragment() {

    private var _binding: FragmentEnterpriseDetailBinding? = null
    private val binding get() = _binding!!

    private val args: EnterpriseDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentEnterpriseDetailBinding
        .inflate(inflater, container, false)
        .apply { enterprise = args.enterprise }
        .also { _binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
    }

    private fun setUpToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            (requireActivity() as? AppCompatActivity)?.onSupportNavigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
