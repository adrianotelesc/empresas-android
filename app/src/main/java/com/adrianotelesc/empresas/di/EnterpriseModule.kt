package com.adrianotelesc.empresas.di

import com.adrianotelesc.empresas.data.EnterpriseRepository
import com.adrianotelesc.empresas.data.EnterpriseRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
abstract class EnterpriseModule {
    @Binds
    abstract fun bindEnterpriseRepository(impl: EnterpriseRepositoryImpl): EnterpriseRepository
}
