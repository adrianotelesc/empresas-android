package com.adrianotelesc.empresas.di

import com.adrianotelesc.empresas.api.EmpresasService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object ApiModule {
    @Provides
    fun provideEmpresaService(): EmpresasService = EmpresasService.newInstance()
}
