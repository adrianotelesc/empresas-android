package com.adrianotelesc.empresas.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.adrianotelesc.empresas.data.Result
import com.adrianotelesc.empresas.data.UserRepository
import com.adrianotelesc.empresas.data.succeeded
import com.adrianotelesc.empresas.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val emailAddress = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _signInEvent = MutableLiveData<Event<Unit>>()
    val signInEvent: LiveData<Event<Unit>> = _signInEvent

    val signInResult = _signInEvent.switchMap {
        liveData {
            emit(Result.Loading)
            emitSource(userRepository.signIn(emailAddress.value, password.value))
        }
    }

    val signInCompletedEvent = signInResult.switchMap {
        liveData {
            emit(Event(it.succeeded))
        }
    }

    fun signIn() {
        _signInEvent.value = Event(Unit)
    }
}
