package com.adrianotelesc.empresas.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.adrianotelesc.empresas.data.succeeded
import com.adrianotelesc.empresas.databinding.FragmentSignInBinding
import com.adrianotelesc.empresas.util.EventObserver
import com.adrianotelesc.empresas.util.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInFragment : Fragment() {

    private var _binding: FragmentSignInBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SignInViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentSignInBinding
        .inflate(inflater, container, false)
        .apply {
            viewmodel = viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        .also { _binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSignInButton()
        observeSignInResult()
    }

    private fun setUpSignInButton() {
        binding.signInButton.setOnClickListener {
            binding.emailTextField.editText?.clearFocus()
            binding.passwordTextField.editText?.clearFocus()
            requireActivity().hideKeyboard(it)

            viewModel.signIn()
        }
    }

    private fun observeSignInResult() {
        viewModel.signInCompletedEvent.observe(
            viewLifecycleOwner,
            EventObserver { succeeded ->
                if (succeeded) {
                    findNavController().navigate(SignInFragmentDirections.showEnterprises())
                }
            }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
