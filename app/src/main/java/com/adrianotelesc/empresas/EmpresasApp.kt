package com.adrianotelesc.empresas

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EmpresasApp : Application()
