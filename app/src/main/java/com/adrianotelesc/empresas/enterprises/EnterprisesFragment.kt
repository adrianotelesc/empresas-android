package com.adrianotelesc.empresas.enterprises

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.adrianotelesc.empresas.R
import com.adrianotelesc.empresas.data.Result
import com.adrianotelesc.empresas.databinding.FragmentEnterprisesBinding
import com.adrianotelesc.empresas.util.longSnack
import com.adrianotelesc.empresas.util.onQueryTextSubmit
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EnterprisesFragment : Fragment() {

    private var _binding: FragmentEnterprisesBinding? = null
    private val binding get() = _binding!!

    private val viewModel: EnterprisesViewModel by viewModels()

    private val adapter = EnterpriseAdapter().apply {
        setOnEnterpriseClick {
            findNavController().navigate(EnterprisesFragmentDirections.seeEnterpriseDetail(it))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentEnterprisesBinding
        .inflate(inflater, container, false)
        .apply {
            viewmodel = viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        .also { _binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        setUpRecyclerView()
        observeEnterprises()
    }

    private fun setUpToolbar() {
        with(binding.toolbar) {
            inflateMenu(R.menu.enterprises_menu)
            (menu.findItem(R.id.search)?.actionView as? SearchView)?.apply {
                maxWidth = Integer.MAX_VALUE
                onQueryTextSubmit { query -> viewModel.filter(query); true }
            }
        }
    }

    private fun setUpRecyclerView() {
        binding.enterprises.adapter = adapter
    }

    private fun observeEnterprises() {
        viewModel.enterprises.observe(viewLifecycleOwner) { result ->
            if (result is Result.Success) {
                adapter.submitList(result.data)
                binding.empty.visibility =
                    if (result.data.isNotEmpty()) View.INVISIBLE else View.VISIBLE
            }

            if (result is Result.Error) {
                longSnack(R.string.error_message)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
