package com.adrianotelesc.empresas.enterprises

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.adrianotelesc.empresas.data.EnterpriseRepository
import com.adrianotelesc.empresas.data.Result
import com.adrianotelesc.empresas.data.UserRepository
import com.adrianotelesc.empresas.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class EnterprisesViewModel @Inject constructor(
    private val userRepo: UserRepository,
    private val enterpriseRepo: EnterpriseRepository
) : ViewModel() {

    private val _filterEvent = MutableLiveData<Event<String>>()
    val filterEvent: LiveData<Event<String>> = _filterEvent

    val enterprises = _filterEvent.switchMap {
        liveData {
            emit(Result.Loading)
            emitSource(enterpriseRepo.filter(userRepo.auth, it.peekContent()))
        }
    }

    fun filter(query: String) {
        _filterEvent.value = Event(query)
    }
}
