package com.adrianotelesc.empresas.enterprises

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adrianotelesc.empresas.data.Enterprise
import com.adrianotelesc.empresas.databinding.ListItemEnterpriseBinding
import com.adrianotelesc.empresas.enterprises.EnterpriseAdapter.ViewHolder

class EnterpriseAdapter : ListAdapter<Enterprise, ViewHolder>(EnterpriseDiffCallback()) {

    private var onEnterpriseClick: ((Enterprise) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemEnterpriseBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.binding) {
            enterprise = getItem(position)
            root.setOnClickListener { onEnterpriseClick?.invoke(getItem(position)) }
        }
    }

    fun setOnEnterpriseClick(onItemClick: (Enterprise) -> Unit) {
        this.onEnterpriseClick = onItemClick
    }

    class ViewHolder(val binding: ListItemEnterpriseBinding) : RecyclerView.ViewHolder(binding.root)
}

class EnterpriseDiffCallback : DiffUtil.ItemCallback<Enterprise>() {
    override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem == newItem
}
