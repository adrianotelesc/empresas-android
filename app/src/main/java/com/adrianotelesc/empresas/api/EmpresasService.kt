package com.adrianotelesc.empresas.api

import com.adrianotelesc.empresas.data.EnterprisesResponse
import com.adrianotelesc.empresas.data.UserCredential
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Query

interface EmpresasService {

    @POST("api/v1/users/auth/sign_in")
    suspend fun userSignIn(@Body body: UserCredential): Response<Unit>

    @GET("api/v1/enterprises")
    suspend fun filterEnterprises(
        @HeaderMap auth: Map<String, String>,
        @Query("name") name: String
    ): Response<EnterprisesResponse>

    companion object {

        const val BASE_URL = "https://empresas.ioasys.com.br"

        fun newInstance(): EmpresasService =
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EmpresasService::class.java)
    }
}
