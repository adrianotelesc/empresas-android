package com.adrianotelesc.empresas.data

data class UserCredential(val email: String?, val password: String?)
