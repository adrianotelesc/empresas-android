package com.adrianotelesc.empresas.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.adrianotelesc.empresas.api.EmpresasService
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

interface UserRepository {

    val auth: Map<String, String>

    fun signIn(emailAddress: String?, password: String?): LiveData<Result<Unit>>
}

@Singleton
class UserRepositoryImpl @Inject constructor(
    private val service: EmpresasService
) : UserRepository {

    override val auth = mutableMapOf<String, String>()

    override fun signIn(emailAddress: String?, password: String?): LiveData<Result<Unit>> =
        liveData(Dispatchers.IO) {
            val result = try {
                val response =
                    service.userSignIn(UserCredential(emailAddress, password))
                if (response.isSuccessful) {
                    auth[ACCESS_TOKEN] = response.headers()[ACCESS_TOKEN].toString()
                    auth[CLIENT] = response.headers()[CLIENT].toString()
                    auth[UID] = response.headers()[UID].toString()

                    Result.Success(Unit)
                } else {
                    Result.Error(Exception())
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
            emit(result)
        }

    companion object {
        private const val ACCESS_TOKEN = "access-token"
        private const val CLIENT = "client"
        private const val UID = "uid"
    }
}
