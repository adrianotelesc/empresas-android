package com.adrianotelesc.empresas.data

data class EnterprisesResponse(val enterprises: List<Enterprise>)
