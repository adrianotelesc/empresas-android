package com.adrianotelesc.empresas.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Enterprise(
    val id: Long,
    @SerializedName("enterprise_name") val name: String,
    val description: String,
    @SerializedName("photo") val photoUrl: String,
    @SerializedName("country") val countryIsoCode: String,
    @SerializedName("city") val cityName: String,
    @SerializedName("enterprise_type")
    val type: Type
) : Parcelable {
    @Parcelize
    data class Type(@SerializedName("enterprise_type_name") val name: String) : Parcelable
}
