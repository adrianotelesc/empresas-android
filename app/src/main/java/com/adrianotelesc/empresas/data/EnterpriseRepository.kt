package com.adrianotelesc.empresas.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.adrianotelesc.empresas.api.EmpresasService
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EnterpriseRepositoryImpl @Inject constructor(
    private val service: EmpresasService
) : EnterpriseRepository {
    override fun filter(
        auth: Map<String, String>,
        query: String
    ): LiveData<Result<List<Enterprise>>> = liveData(Dispatchers.IO) {
        val result = try {
            val response = service.filterEnterprises(auth, query)
            if (response.isSuccessful) {
                Result.Success(response.body()?.enterprises.orEmpty())
            } else {
                Result.Error(Exception())
            }
        } catch (e: Exception) {
            Result.Error(e)
        }
        emit(result)
    }
}

interface EnterpriseRepository {
    fun filter(
        auth: Map<String, String>,
        query: String
    ): LiveData<Result<List<Enterprise>>>
}
